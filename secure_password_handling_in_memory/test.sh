#!/bin/bash
password="very_secret"

echo "$password" | ruby "$1" &
child_pid=$!
gcore "$child_pid"
lines_with_password=$(strings "core.$child_pid" | grep "$password" | wc -l)
rm core*
echo $lines_with_password
exit $lines_with_password
