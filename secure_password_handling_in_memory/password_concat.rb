#!/usr/bin/ruby
#
require 'stringio'
require 'tempfile'


class String
  # create io string with same size of pass, filled with 0x00
  # override pass with 0x00
  def clear
    bytesize = self.bytesize
    io = StringIO.new("\0" * bytesize)
    io.read(bytesize, self)
  end

end



password = "Fooo" + "bar"
wrap = "password=#{password}"

# TODO: Dont use Tempfile!
#f = Tempfile.new("creds-", ".")
#f.puts(wrap)
#f.flush
#f.close

# TODO: this works fine
#File.open("creds", "w") do |f|
#  f.puts(wrap)
#  f.flush
#end

#f = File.open("creds", "w")
#f.puts(wrap)
#f.flush
#f.close

Tempfile.new("creds-", ".") do |f|
  f.puts(wrap)
  f.flush
end

password.clear
wrap.clear

require 'pry'; binding.pry
