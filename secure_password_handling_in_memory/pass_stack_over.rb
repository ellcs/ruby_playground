require 'stringio'

class StringClearer

  # create io string with same size of pass, filled with 0x00
  # override pass with 0x00
  def self.clear(string)
    bytesize = string.bytesize
    io = StringIO.new("\0" * bytesize)
    io.read(bytesize, string)
  end

end

pass = ""
$stdin.sysread(256, pass) # assuming a line-buffered terminal
StringClearer.clear(pass)
#io = StringIO.new("\0" * pass.bytesize)
#io.read(pass.bytesize, pass)
p pass

sleep
