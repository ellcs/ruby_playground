#!/usr/bin/ruby
# https://bugs.ruby-lang.org/issues/5741
#
# systemd in archlinux store coredumps in /var/lib/systemd/coredump/
require 'stringio'

pass = ""
# assuming a line-buffered terminal
$stdin.sysread(256, pass)
p pass

pass.clear
Process.kill(:ABRT, $$)
sleep # wait for SIGABRT to hit us

# create io string with same size of pass, filled with 0x00
io = StringIO.new("\0" * pass.bytesize)
# override pass with 0x00
io.read(pass.bytesize, pass)
p pass

Process.kill(:ABRT, $$)
sleep # wait for SIGABRT to hit us

