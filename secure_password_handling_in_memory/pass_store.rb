#!/usr/bin/ruby
# https://bugs.ruby-lang.org/issues/5741
#
# systemd in archlinux store coredumps in /var/lib/systemd/coredump/
#
# $ gdb ruby
# > r %
# RECV ABRT
# > generate-core-file
require 'stringio'

class StringClearer

  # create io string with same size of pass, filled with 0x00
  # override pass with 0x00
  def clear(string)
    bytesize = string.bytesize
    io = StringIO.new("\0" * bytesize)
    io.read(bytesize, string)
  end

end

pass = ""
# assuming a line-buffered terminal
$stdin.sysread(256, pass)
StringClearer.new.clear(pass)
p pass
sleep

