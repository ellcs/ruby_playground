# Usage: redis-cli publish message hello

require 'sinatra'
require 'redis'

conns = []

get '/' do
  erb :index
end

get '/subscribe' do
  content_type 'text/event-stream'
  stream(:keep_open) do |out|
    conns << out
    out.callback { conns.delete(out) }
  end
end

1000.times do
  conns.each do |out|
    out << "event: hi\n\n"
    out << "data: hi\n\n"
  end
end

#Thread.new do
#  redis = Redis.connect
#  redis.psubscribe('message', 'message.*') do |on|
#    on.pmessage do |match, channel, message|
#      channel = channel.sub('message.', '')
#
#      conns.each do |out|
#        out << "event: #{channel}\n\n"
#        out << "data: #{message}\n\n"
#      end
#    end
#  end
#end

__END__

@@ index
<!DOCTYPE html>
<html>
<head>
  <title>My first Vue app</title>
</head>
<body>

  <article id="log"></article>

  <script>
    var source = new EventSource('/subscribe');

    source.onmessage = function (event) {
      alert("hi");
    };

    // source.addEventListener('message', function (event) {
    //   alert("hi");
    //   log.innerText += '\n' + event.data;
    // }, false);
  </script>
</body>
</html>
