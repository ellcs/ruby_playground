require 'sinatra'
require 'sinatra/hijacker'
register Sinatra::Hijacker

websockets = []

websocket '/updated' do
    websockets << ws
    ws.onmessage  do |msg|
        puts msg
    end
    ws.onclose do
        websockets.delete(ws)
    end
    "Done"
end

get '/' do
  erb :medium_select
end
