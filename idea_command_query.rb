#                               +-------------+
#                               |             |
#                           +---+   command   |
#                           |   |             |
#          +-----------+    |   +-------------+
#          |   model   <----+
#          +-----------+    |   +-------------+
#                           |   |             |
#                           +---+   query     |
#                               |             |
#                               +-------------+
#
class Model

  # creates subclass 'Repository/Factory'
  include CommandQuery

  command       :name
  query         :name

end

# command contains mutable methods only
# query contains immutable methods only
# model contains both
model, query, command = Model::Repository::new

assert query == Model::Repository::query(model)
assert query == Model::Repository::query(query)
assert query == Model::Repository::query(command)

assert command == Model::Repository::command(model)
assert command == Model::Repository::command(query)
assert command == Model::Repository::command(command)
