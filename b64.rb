BASE64_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".freeze

def encode_first(str)
  candidates = [str.bytes, [nil] + str.bytes, [nil, nil] + str.bytes] 
  candidates.map do |candidate| accu = ''
    c = 0 # count outputted characters
    candidate.each_slice(3) do |buf|
       pad = buf # padding for this group of 4 characters
      while buf.length < 3
        pad << nil 
      end
      buf = pad.map { |b| ((b.nil?) && 0) || b }
      #require 'pry'; binding.pry

      group24 = (buf[0] << 16) | (buf[1] << 8) | buf[2] # current 3 bytes as a 24 bit value
      encoded = ''
      encoded << BASE64_CHARS[(group24 >> 18) & 0x3f] # read the 24 bit value 6 bits at a time
      encoded << BASE64_CHARS[(group24 >> 12) & 0x3f]
      encoded << BASE64_CHARS[(group24 >> 6) & 0x3f]
      encoded << BASE64_CHARS[(group24 >> 0) & 0x3f]
      if false
      elsif pad[0].nil? && pad[1].nil?
        encoded[0] = '='
        encoded[1] = '=' 
        # encoded[2] 4bit...
      elsif pad[0].nil?
        encoded[0] = '='
        # encoded[1] 2bit...
      elsif pad[1].nil? && pad[2].nil?
        # encoded[1] 4bit...
        encoded[2] = '=' 
        encoded[3] = '='
      elsif pad[2].nil?
        encoded[3] = '='
      end
      accu << encoded
    end
    accu
  end
end

def encode_regex(str)
  candidates = [str.bytes, [nil] + str.bytes, [nil, nil] + str.bytes] 
  candidates.map do |candidate|
    accu = ''
    c = 0 # count outputted characters
    candidate.each_slice(3) do |buf|
       symbolic = buf # symbolicding for this group of 4 characters
      while buf.length < 3
        symbolic << nil 
      end
      buf = symbolic.map { |b| ((b.nil?) && 0) || b }

      group24 = (buf[0] << 16) | (buf[1] << 8) | buf[2] # current 3 bytes as a 24 bit value
      encoded = ''
      encoded << BASE64_CHARS[(group24 >> 18) & 0x3f] # read the 24 bit value 6 bits at a time
      encoded << BASE64_CHARS[(group24 >> 12) & 0x3f]
      encoded << BASE64_CHARS[(group24 >> 6) & 0x3f]
      encoded << BASE64_CHARS[(group24 >> 0) & 0x3f]
      if false
      elsif symbolic[0].nil? && symbolic[1].nil?
        encoded[0] = '='
        encoded[1] = '=' 
        # encoded[2] 4bits..
        pos = BASE64_CHARS.index(encoded[2])
        possibilities = (4**2).times.map.with_index(0x3f - 16) { |i| BASE64_CHARS[(pos ^ i)] }
        encoded[2] = sprintf("(%s)", possibilities.join("|"))
      elsif symbolic[0].nil?
        require 'pry'; binding.pry
        encoded[0] = '='
        # encoded[1] first two bits...
        pos = BASE64_CHARS.index(encoded[1])
        possibilities = (2**2).times.map.with_index(0) { |i| BASE64_CHARS[(pos ^ (i << 4))] }
        encoded[1] = sprintf("(%s)", possibilities.join("|"))
      end

      if symbolic[1].nil? && symbolic[2].nil?
        # encoded[1] 4bit...
        encoded[2] = '=' 
        encoded[3] = '='
        pos = BASE64_CHARS.index(encoded[1])
        possibilities = (4**2).times.map.with_index(0x00) { |i| BASE64_CHARS[(pos ^ i)] }
        encoded[1] = sprintf("(%s)", possibilities.join("|"))
      elsif symbolic[2].nil?
        # encoded[3] last two bits...
        # four bits correct; two bits incorrect: 2**2
        encoded[3] = '='
        pos = BASE64_CHARS.index(encoded[2])
        possibilities = (2**2).times.map.with_index(0x00) { |i| BASE64_CHARS[(pos ^ i)] }
        encoded[2] = sprintf("(%s)", possibilities.join("|"))
      end
      accu << encoded
    end
    accu
  end
end

require 'pry'; binding.pry
