class Thing

  extend Forwardable

  attr_accessor :name

  def_delegator :name, :to_s

end

t = Thing.new
t.name = "james"
puts t.to_s
