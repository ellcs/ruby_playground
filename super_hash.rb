class MyHash < Hash

  def [](a)
    self.fetch(a)
  end

end

h = MyHash.new
h[:a] = 1
h[:b] = 2

h[:c]
