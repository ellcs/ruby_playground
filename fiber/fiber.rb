fiber = Fiber.new do |first|
  second = Fiber.yield first + 2
  #        Fiber.yield first + 2 => :a
end

puts fiber.resume 10
puts fiber.resume :a
puts fiber.resume 18
