require 'tempfile'
require 'securerandom'

content = SecureRandom.hex
file_path = Tempfile.open("delete_me") do |file|
              file.puts(content)
              file.path
            end

require 'pry'; binding.pry
