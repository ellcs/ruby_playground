require 'benchmark'

# generate random int array
a = []
1000.times do
  a << (rand * 1000).to_i
end

# lamda for even int
even = Proc.new { |i| i % 2 == 0 }

select_time = Benchmark.realtime do
  10000.times do
    a.select(&even)
  end
end

def call_select_return(a, even)
    a.select do |i|
      return i if even.call(i)
    end
end

select_return_time = Benchmark.realtime do
  10000.times do
    call_select_return(a, even)
  end
end

find_time = Benchmark.realtime do
  10000.times do
    a.find(&even)
  end
end

puts "select_time: %.9f" % select_time
puts "select_return_time: %.9f" % select_return_time
puts "find_time: %.9f" % find_time
