require 'benchmark'
require 'sequel'


# the function must be defined in such a place
# ... so as to "catch" the binding of the vars ... cheesy
# otherwise we're kinda stuck with the extra param on the caller
@_binding = binding
def write_pair(p, b = @_binding)
  eval("
    local_variables.each do |v|
      if eval(v.to_s + \".object_id\") == " + p.object_id.to_s + "
        puts v.to_s + ': ' + \"" + p.to_s + "\"
      end
    end
  " , b)
end

DB = Sequel.sqlite # memory database, requires sqlite3
DB.create_table :items do
  primary_key :id
  integer :num
end
db_items = DB[:items]

insert_sequel = Benchmark.realtime do
  i = 0
  1000.times do
    db_items.insert(id: i, num: i)
    i = i + 1
  end
end

array_items = []
insert_push = Benchmark.realtime do
  i = 0
  1000.times do
    array_items << [i, i]
    i = i + 1
  end
end

select_odd_sequel = Benchmark.realtime do
  db_items.where{{(num.sql_number % 2) => 0}}
end

select_odd_sequel = Benchmark.realtime do
  DB["SELECT * FROM items WHERE num % 2 = 0"]
end

select_odd_ruby = Benchmark.realtime do
  array_items.select {|i| i[1] % 2 == 0 }
end

write_pair(insert_sequel)
write_pair(insert_push)
write_pair(select_odd_sequel)
write_pair(select_odd_ruby)
