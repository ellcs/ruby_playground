require 'benchmark'

def i_will_yield
  yield
end

def i_will_call(&b)
  b.call
end

yield_result = Benchmark.realtime do
  1_000_000.times do
    i_will_yield { 1 + 1 }
  end
end

call_result = Benchmark.realtime do
  1_000_000.times do
    i_will_call { 1 + 1 }
  end
end

puts "call:  %.9f" % call_result
puts "yield: %.9f" % yield_result

# $ ruby call_vs_yield.rb
# call:  0.419539942
# yield: 0.076669322
