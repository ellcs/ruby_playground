require 'benchmark'

data = (0..50_000_000)

Benchmark.bm do |x|
  x.report(:find) { data.find {|number| number > 40_000_000 } }
  x.report(:bsearch) { data.bsearch {|number| number > 40_000_000 } }
end

puts "#{data.find {|number| number > 40_000_000 }}"
puts "#{data.bsearch {|number| number > 40_000_000 }}"
