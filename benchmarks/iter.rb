require 'benchmark'

data = (0..50_000_000)

Benchmark.bm do |x|
  x.report(:for_1) { data.select { |i| i % 4 == 0 }; data.select { |i| i % 3 == 0 }}
  x.report(:for_2) do
    data.reduce ({:a => [], :b => []}) do |a, i|
      if i % 4 == 0
        a[:a] << x
      elsif i % 3 == 0
        a[:b] << i
      end
      a
    end
  end
end

puts "#{data.find {|number| number > 40_000_000 }}"
puts "#{data.bsearch {|number| number > 40_000_000 }}"
