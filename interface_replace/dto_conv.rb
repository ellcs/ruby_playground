require 'forwardable'

class Original
  attr_accessor :name
end


class Dto

  extend Forwardable

  def_delegator :original, :name, :name

  protected

  attr_accessor :original
end

class DtoConverter < Dto
  def original(dto)
    dto.original
  end

  def to_dto(original)
    dto = Dto.new
    dto.original = original
    dto
  end
end

# Ein Converter welcher eine Instanz 'original' wrapped.
# Original ist nicht durch den dto änderbar!
def run
  o = Original.new
  o.name = "james"

  c = DtoConverter.new
  d = c.to_dto(o)

  # lesen ja
  d.name

  # schreiben knallt
  #d.name = "antuan"

  # original bekommen, ja
  c.original(d)
  require 'pry'; binding.pry
end

run
