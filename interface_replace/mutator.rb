require 'forwardable'

# Disjunkte trennung von read obj und write obj

class Original
  attr_reader :name

  protected

  attr_writer :name
end

class OriginalMutator < Original

  extend Forwardable

  undef_method :name

  def initialize(original)
    @original = original
  end

  def_delegator :@original, :name=, :name=
end

# Ein Converter welcher eine Instanz 'original' wrapped.
# Original ist nicht durch den dto änderbar!
def run
  # original darf
  o = Original.new
  m = OriginalMutator.new(o)
  m.name = "james"

  puts "true? #{o.name == "james"}"
  require 'pry'; binding.pry
end

run

