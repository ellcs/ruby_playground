require 'forwardable'

# can read and write
class Original
  attr_accessor :name
  attr_accessor :age
end

class OriginalReader

  extend Forwardable

  def initialize(original)
    @original = original
  end

  def_delegator :@original, :name, :name
  def_delegator :@original, :age,  :age
end

# Ein Converter welcher eine Instanz 'original' wrapped.
# Original ist nicht durch den dto änderbar!
def run
  # original darf
  o = Original.new
  m = OriginalMutator.new(o)
  m.name = "james"

  puts "true? #{o.name == "james"}"
  require 'pry'; binding.pry
end

run


