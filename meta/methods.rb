#!/usr/bin/ruby
#
require 'set'

class A

  attr_reader   :a
  attr_accessor :b
  def c; end
end

def direct_defined_instance_methods(obj)
  klass = if obj.is_a?(Class)
            obj
          else
            obj.class
          end
  ancestors = klass.ancestors
  ancestors = ancestors[1..-1]
  ancestor_methods = ancestors.reduce(Set.new) do |accu, ancestor|
                       accu.merge(Set.new(ancestor.instance_methods))
                     end
  klass.instance_methods.reject { |method| ancestor_methods.include?(method)}
end



def mutator_instance_methods(obj)
end

def assert_equ(result, expected, msg="")
  unless result == expected
    puts msg
    raise "not working"
  end
end

def test
  assert_equ Set.new(direct_defined_instance_methods(A.new)), Set[:a, :b, :b=, :c]
  assert_equ Set.new(direct_defined_instance_methods(A)), Set[:a, :b, :b=, :c]
  require 'pry'; binding.pry
end

test

