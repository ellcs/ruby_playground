require 'set'

module IncludeMe

  @@abstract_map = Set.new

  def self.included(base)
    @@abstract_map << base
  end

  def self.map
    @@abstract_map
  end
end

class A
  include IncludeMe
end

class B
  include IncludeMe
end

puts "#{IncludeMe.map}"
require 'pry'; binding.pry
