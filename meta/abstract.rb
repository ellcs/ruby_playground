require 'set'

class Class
  def descendants
    ObjectSpace.each_object(::Class).select {|klass| klass < self }
  end

  # Hook method, since all Classes default to 'nonabstract'
  def abstract?; false; end
end

module Abstract

  # Use Exception instead of StandardError because if a method is not
  # implemented, it is a programmers mistake.
  class NotImplementedError < Exception
  end

  # maps from abstract_classes to abstract_methods
  @@abstract_classes = {}

  # simple hook, which registeres the abstract class and adds class methods
  # to that freshly registered abstract class.
  def self.included(base)
    @@abstract_classes[base] = Set.new
    base.class_eval do
      def self.abstract(sym)
        Abstract.add(self, sym)
      end

      def self.abstract?
        Abstract.class?(self)
      end
    end
  end

  # currently checks if direct descendant implements all abstract methods.
  def self.check_implemented!
    @@abstract_classes.each do |abstract_class, abstract_methods|
      abstract_class.descendants.each do |descendant|
        not_implemented_methods = abstract_methods - descendant.instance_methods
        unless not_implemented_methods.empty?
          method_list = not_implemented_methods.to_a.join(", ")
          raise NotImplementedError.new("Implement #{descendant}#\{#{method_list}}")
        end
      end
    end
  end

  def self.debug
    @@abstract_classes
  end

  private

  # checks if given class is an abstract class.
  # raises an Exception when
  def self.class?(klass)
    raise Exception.new "Given object is not a class: #{klass}" unless klass.is_a?(Class)
    @@abstract_classes.keys.include?(klass)
  end

  def self.add(base, sym)
    @@abstract_classes[base] << sym
  end

end

################################################################################
################################# USAGE ########################################
################################################################################

class A
  include Abstract
  abstract :a
  abstract :b
end

class B < A
  def a; end
  def b; end
end

class C < A
  def a; end
  def b; end
end

# checks if such methods has been defined.
Abstract.check_implemented!

raise "inconsistent!" unless Abstract.class?(A)
raise "inconsistent!" if Abstract.class?(B)
raise "inconsistent!" if Abstract.class?(C)

raise "inconsistent!" unless A.abstract?
raise "inconsistent!" if B.abstract?
raise "inconsistent!" if C.abstract?

puts "If you ran into pry, everything worked well :)"
require 'pry'; binding.pry
