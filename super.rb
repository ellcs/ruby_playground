
class A

  def foo(arg)
    puts "A#foo(#{arg})"
  end

end

class B < A

  def foo
    super("hi")
  end

end

B.new.foo
