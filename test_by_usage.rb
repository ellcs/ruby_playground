require 'singleton'
require 'ostruct'
require 'set'

class UsageStorage

  include Singleton

  attr_accessor :usages

  def initialize
    @usages = Set.new
  end

  def add_normal_usage(klass, method, args, block, result)
    usage = OpenStruct.new
    usage.klass = klass
    usage.method = method
    usage.args = args
    usage.block = block
    usage.result = result
    @usages << usage
  end
end

module TestGenerator
    attr_reader :record, :usage

    def generate_test_by_usage
      @record = true
    end

    def method_added(method)
      @record ||= false
      if @record
        record_usage(method)
        @record = false
      end
      super
    end

    def record_usage(m)
      proxy = Module.new do
        define_method(m) do |*args, &block|
          begin
            result = super(*args, &block)
            UsageStorage.instance.add_normal_usage(self.class, m, args, block, result)
          rescue Exception => ex
            UsageStorage.instance.add_exception_raised(self.class, m, args, block, ex)
            raise ex
          end
          result
        end
      end
      prepend proxy
    end

end


class Usecase

  extend TestGenerator

  generate_test_by_usage
  def test(a,b,c)
    a + b + c
  end

end

Usecase.new.test(1,2,3)
Usecase.new.test(1,2,3) { puts("hi") }
Usecase.new.test("", "asdf", "qwer") { puts("hi") }

require 'pry'; binding.pry
