#!/usr/bin/ruby
#
require 'thread'

class DeviceList

  def initialize
    @device = []
    @mutex = Mutex.new
  end

  def add(device)
    raise ArgumentError unless device.is_a?(String)
    @mutex.synchronize do
      @device << device
    end
  end

  def devices
    @mutex.synchronize do
      @device.pop
    end
  end

end

devices = DeviceList.new

t1 = Thread.new do
  99.times do |i|
    devices.add("#{i}")
  end
end

t2 = Thread.new do
  while true
    puts devices.devices
  end
end

t1.join
t2.join


