class A

  def initialize
    @b = B.new(self)
  end

  def a
    puts "a"
  end

  def b
    @b.b
  end

end

class B

  def initialize(a)
    @a = a
  end

  def b
    puts "b"
    @a.a
  end

end

A.new.b
