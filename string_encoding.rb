#!/usr/bin/ruby
# String-Literate sind immer erstmal UTF-8. (spätestens ab Ruby-2.0)
#
# ASCII ist nur sauber wenn es lediglich amerikanische Zeichen und keine
# Umlaute enthält.
#
# force_encoding setzt nur die Encoding Meta-Info. Der interne Byte-Array des
# Strings muss von UTF-8 nach ASCII nicht verändert werden, weil jedes saubere
# ASCII-Zeichen auch sauberes UTF-8 ist.
# Note: force_encoding modifies the existing object!
valid_ascii8bit = 'valid'.force_encoding('ASCII-8BIT')

invalid_ascii8bit = 'dirty ä'.force_encoding('ASCII-8BIT')

# Array::pack('H*') gibt einen String mit ASCII-8BIT Encoding als
# Meta-Info zurück.
totally_invalid_ascii8bit = ('invalid ä and char '.force_encoding('ASCII-8BIT') + ['81'].pack('H*'))

# Das interne byte array ist sowohl valides UTF-8, als auch valides ASCII.
# Lediglich die Encoding Meta-Info besagt, dass der String als UTF-8
# interpretiert werden soll.
valid_a_compatible_utf8 = 'valid'

# Das interne byte array enthält ein "ä", für welches es in ASCII keine gültige
# Interpretation gibt.
# Dieser String ist also wirklich nur gültiges UTF-8 und kein gültiges ASCII.
valid_a_incompatible_utf8 = 'valid ä'

# Enthält Zeichen "\x81", welches kein gültiges UTF-8 ist.
invalid_utf8 = ('invalid char ' + ['81'].pack('H*')).force_encoding('UTF-8')

puts (valid_ascii8bit + valid_a_compatible_utf8).encoding
# => ASCII-8BIT
# Das Encoding des ersten Operant wird bevorzugt.

puts (valid_a_compatible_utf8 + valid_ascii8bit).encoding
# => UTF-8
# Das Encoding des ersten Operant wird bevorzugt.

puts (valid_a_compatible_utf8 + invalid_ascii8bit).encoding
# => ASCII-8BIT
# Das Encoding des ersten Operanten sollte gewinnen. Aber invalid_ascii8bit
# enthält ein Zeichen "ä" für welches es keine gültige ASCII-Interpretation
# gibt und welches deshalb nicht nach UTF-8 konvertiert werden kann. Stattdessen
# passt Ruby deshalb des ersten Operanten an, so dass er zum zweiten passt.

puts (valid_ascii8bit + valid_a_incompatible_utf8).encoding
# => UTF-8
# Das Encoding des ersten Operanten sollte gewinnen.
# Aber valid_a_incompatible_utf8 enthält ein Zeichen "ä" für welches es keine
# gültige ASCII-Variante gibt und weshalb der zweite Operant nicht zu ASCII
# konvertiert werden kann. Stattdessen wird auch hier der erste Operant
# angepasst, so dass er zum zweiten passt.

begin
	puts (invalid_ascii8bit + valid_a_incompatible_utf8).encoding
rescue Encoding::CompatibilityError => ex
	puts ex.class
end
# => Encoding::CompatibilityError
# invalid_ascii8bit kann nicht nach UTF-8 konverviert werden.
# valid_a_incompatible_utf8 kann nicht zu ASCII konvertiert werden.

begin
	invalid_utf8.sub('invalid', 'valid').encoding
rescue ArgumentError => ex
	puts ex.class
end
# => ArgumentError
# Auf UTF-8 Strings mit ungültigen Zeichen können viele String-Operationen
# nicht ausgeführt werden.

search_ascii8bit = 'invalid ä'.force_encoding('ASCII-8BIT')
replace_ascii8bit = 'invalid - but manageable - ä'.force_encoding('ASCII-8BIT')
puts totally_invalid_ascii8bit.sub(search_ascii8bit, replace_ascii8bit).encoding
# => ASCII-8BIT
# Auf Strings mit ASCII-8BIT als Encoding Meta-Info gelingen jedoch alle
# String-Operationen. Vorrausgesetzt, alle Operanden sind
# ebenfalls ASCII-8BIT.

begin
	search_utf8 = 'invalid ä'
	replace_utf8 = 'invalid - but manageable - ä'
	puts totally_invalid_ascii8bit.sub(search_utf8, replace_utf8).encoding
rescue Encoding::CompatibilityError => ex
	puts ex.class
end
# => Encoding::CompatibilityError
# ... ansonsten knallt es auch hier. (siehe vorheriges Beispiel)
